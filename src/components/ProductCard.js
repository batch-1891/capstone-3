// import { useState, useEffect } from 'react'
import { Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function ProductCard({productProp}) {


    // Deconstruct the course properties into their own variables
    const {name, description, price, _id} = productProp;


    return (
            <Card>
              <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>Php {price}</Card.Text>
                <Button variant="primary" as={Link} to={`/products/${_id}`}>See Details</Button>
              </Card.Body>
            </Card>
            
        )
}

