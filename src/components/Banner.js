import { Button, Row, Col } from 'react-bootstrap';
import Carousel from 'react-bootstrap/Carousel';
import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
export default function Banner() {
	return(
		<Row className="mx-auto mt-4">
			<Col>
				<Container>
					<Row>
						<Col>
							<Carousel>

								<Carousel.Item>
									<img
									      className="d-block w-100"
									      src="https://th.bing.com/th/id/R.6b544bb234a7c7fbc9baf0f68de2052b?rik=DK83indXtyIu2w&riu=http%3a%2f%2fanalyzingmarket.com%2fwp-content%2fuploads%2f2020%2f11%2fApple-foldable-phone.jpg&ehk=2flaDbUQtb%2fzrviSBConP9ZvA8JDVhds%2fyfp0BD58Z8%3d&risl=&pid=ImgRaw&r=0"
									      alt="Phone"
									    />
									    
								</Carousel.Item>
								<Carousel.Item>
									<img
									      className="d-block w-100"
									      src="https://www.techgenyz.com/wp-content/uploads/2021/07/Samsung-S21-Ultra-5G-1200x675.jpg"
									      alt="Phone"
									    />
									   
								</Carousel.Item><Carousel.Item>
									<img
									      className="d-block w-100"
									      src="https://i.pinimg.com/originals/df/37/ff/df37ff1c8e62643abc8d3da49be2a537.jpg"
									      alt="Phone"
									    />
									    
								</Carousel.Item>
							</Carousel>
						</Col>
					</Row>
				</Container>
			</Col>
		</Row>
		)
}
