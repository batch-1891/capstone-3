import {useState, useEffect, useContext} from 'react'
import {Button, Container} from 'react-bootstrap'
import {Link, Navigate, useNavigate} from 'react-router-dom'
import AdminProductCard from './AdminProductCard'
import UserContext from '../UserContext'

export default function AdminProducts () {

	const {user} = useContext(UserContext)

	const [adminProducts, setAdminProducts] = useState([])


	useEffect(()=>{

			fetch('http://localhost:4000/products', {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {

				setAdminProducts(data.map(adminProduct => {
					return (

							<AdminProductCard key={adminProduct.id} adminProductProp={adminProduct} />

						)
				}))
			})

		

	},[])

	

	return (

			(user.id !== null) ?
				(user.isAdmin == true) ?
					<>

						<h1 className="text-center m-4">Services</h1>
							
						<Container className="p-3 justify-content-end ml-auto">
							<Button variant="primary" as={Link} to='/addProduct'>Add Product</Button>
						</Container>
						
						{adminProducts}
					</>
				:
					<Navigate to="*" />
			:
				<Navigate to="*" />
		)	
}
