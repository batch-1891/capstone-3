import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col} from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'


export default function ProductView() {

	const { user } = useContext(UserContext);

	//Allow us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const navigate = useNavigate(); //useHistory

	//The "useParams"
	const {productId} = useParams()

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [quantity, setQuantity] = useState(1)


	function decrementCount () {
		setQuantity(decrement => decrement-1)
	}

	function incrementCount () {
		setQuantity(increment => increment+1)
	}


	const order = (productId) => {

		fetch(`http://localhost:4000/users/order`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity,
				productCost: price
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === true) {
				Swal.fire({
					title:"Success Enrolled",
					icon: "success",
					text: "You have successfully enrolled for this course."
				})

				navigate("/products")

			} else {
				Swal.fire({
					title:"Something went wrong",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
	}

	useEffect(() => {


		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			

		})

	}, [productId])

	return (

		<Container className="mt-3">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
            			<Card.Body className="text-center">
                			<Card.Title>{name}</Card.Title>
                			<Card.Subtitle>Description:</Card.Subtitle>
                			<Card.Text>{description}</Card.Text>
               	    		<Card.Subtitle>Price:</Card.Subtitle>
               	 			<Card.Text>PhP {price}</Card.Text>
               	    		<Button onClick={decrementCount}>-</Button>
				            <span className="mx-2">{quantity}</span>
				            <Button onClick={incrementCount}>+</Button>


                   		    {
                   		    	user.id !== null ?

                   		    	<Button className="mx-2" variant="primary" onClick={() => order(productId)}>Buy</Button>

                   		    	:

                   		    	<Link className="btn btn-danger mx-2" to="/login" > Log in to buy </Link>
                   		    }
                     	</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

		)
}